import express from "express";
import bodyParser from "body-parser";
const app = express();

app.get("/hello", function (req, res) {
  res.send("Hello 3TECH");
});

app.use(bodyParser.json());
app.listen(3000);
